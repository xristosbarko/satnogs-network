[metadata]
name = satnogs-network
url = https://gitlab.com/librespacefoundation/satnogs/satnogs-network
author = SatNOGS project
author_email = dev@satnogs.org
license = AGPLv3
classifiers =
    Development Status :: 4 - Beta
    Environment :: Web Environment
    Framework :: Django
    Intended Audience :: Telecommunications Industry
    Intended Audience :: Science/Research
    License :: OSI Approved :: GNU Affero General Public License v3
    Operating System :: POSIX :: Linux
    Programming Language :: Python
    Programming Language :: Python :: 3
    Topic :: Communications :: Ham Radio
    Topic :: Internet :: WWW/HTTP
    Topic :: Internet :: WWW/HTTP :: Dynamic Content
description = SatNOGS Network

[options]
packages = find:
include_package_data = True
install_requires =
    # Basic
    Django~=4.0.0
    django-shortuuidfield~=0.1.0
    celery~=5.2.0
    Pillow~=9.1.0
    tinytag~=1.8.1
    # Storage and Archiving
    internetarchive~=3.0.0
    django-storages~=1.12.0
    boto3~=1.21.0
    # Deployment
    mysqlclient~=2.1.0
    # pinning for https://github.com/benoitc/gunicorn/pull/2581
    eventlet==0.30.2
    gunicorn[eventlet]~=19.9.0
    # Cache https://docs.djangoproject.com/en/4.0/topics/cache/#redis
    redis~=4.2.0
    hiredis~=2.0.0
    # Logging
    sentry-sdk~=1.5.0
    # Configuration
    python-decouple~=3.6.0
    dj-database-url~=0.5.0
    python-dotenv~=0.20.0
    # Security
    django-csp~=3.7.0
    # Users
    django-allauth~=0.50.0
    django-crispy-forms~=1.14.0
    social-auth-app-django~=5.0.0
    # Needed by Auth0, https://auth0.com/blog/django-authentication/#Adding-Authentication
    python-jose[cryptography]~=3.3.0
    # Static
    django_compressor~=4.0.0
    # API
    djangorestframework~=3.13.0
    drf-spectacular~=0.22.0
    Markdown~=3.3.0
    django-filter~=21.1
    # Astronomy
    ephem~=4.1.2
    # Unsorted
    objgraph~=3.5.0
    # Debugging
    django-debug-toolbar~=3.2.0

[options.extras_require]
dev =
    sphinx~=6.2.0
    pytest-cov~=3.0.0
    pytest-django~=4.5.0
    pytest-forked~=1.4.0
    pytest-xdist~=2.5.0
    mock~=4.0.0
    factory-boy~=3.2.0
    tox~=3.24.0

[flake8]
max-complexity = 25
max-line-length = 99
ignore = F403,W504,W503
exclude = network/_version.py,versioneer.py,*/migrations,docs,build,dist,.tox,node_modules,satnogs-network-api-client,media,.virtualenv

[yapf]
column_limit = 99
split_before_first_argument = True
dedent_closing_brackets = True
allow_split_before_dict_value = False

[tool:isort]
use_parentheses = True
skip_glob = */migrations,build,dist,.tox,node_modules,satnogs-network-api-client,media,.virtualenv
skip = _version.py,versioneer.py
known_third_party = factory
line_length = 99
multi_line_output = 2

[tool:pytest]
addopts = -v --cov --cov-report=term-missing
python_files = tests.py
DJANGO_SETTINGS_MODULE = network.settings

# See the docstring in versioneer.py for instructions. Note that you must
# re-run 'versioneer.py setup' after changing this section, and commit the
# resulting files.

[versioneer]
VCS = git
style = pep440
versionfile_source = network/_version.py
versionfile_build = network/_version.py
tag_prefix =
parentdir_prefix =
